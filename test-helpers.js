import sinon from 'sinon'
import 'sinon-as-promised'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'
import sd from 'skin-deep'
import jsdom from 'jsdom'
const expect = chai.expect
chai.use(require('sinon-chai'))
chai.use(chaiEnzyme)

global.sinon = sinon
global.expect = expect
global.sd = sd

const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
const win = doc.defaultView;

global.document = doc;
global.window = win;
global.navigator = {userAgent: 'node.js'};
