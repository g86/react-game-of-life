import { EventEmitter } from 'events'
// import { Dispatcher } from 'flux'
import AppDispatcher from '../dispatchers/AppDispatcher'
import nextGolGeneration from '../helpers/GameOfLifeMutation'
import Bibimbap from 'bibimbap'
import conf from './../constants/Constants'
import CreateMatrix from './../helpers/CreateMatrix'

export const initialCells = conf.CELLS

export class AppStore extends EventEmitter {

  constructor() {
    super()
    this.subscribe(() => this._registerToActions.bind(this))
    this.appState = new Bibimbap({ cells: CreateMatrix(), running: false })
    this.appState.on('commit', () => {
      this.emitChange()
    })
  }

  subscribe(actionSubscribe) {
    this._dispatchToken = AppDispatcher.register(actionSubscribe())
  }

  get dispatchToken() {
    return this._dispatchToken
  }

  emitChange() {
    this.emit('CHANGE')
  }

  addChangeListener(callback) {
    this.on('CHANGE', callback)
  }

  removeChangeListener(callback) {
    this.removeListener('CHANGE', callback)
  }

  _registerToActions(action) {
    console.log("_registerToActions")
    switch (action.actionType) {
      case 'MUTATE_GENERATION':
        this._mutateGeneration()
        break
      case 'RESET':
        this._resetGameOfLife()
        break
      case 'START':
        this._startGameOfLife()
        break
      case 'STOP':
        this._stopGameOfLife()
        break
      default:
        break
    }
  }

  _startGameOfLife() {
    this.appState.cursor().set('running', true)
  }

  _stopGameOfLife() {
    this.appState.cursor().set('running', false)
  }

  _mutateGeneration() {
    let currentGenerationCells = this.appState.cursor().get('cells')
    let nextGenerationCells = nextGolGeneration(currentGenerationCells)
    this.appState.cursor().set('cells', nextGenerationCells)
  }

  _resetGameOfLife() {
    this.appState.cursor()
        .set('running', false)
        .set('cells', CreateMatrix())
  }

  get state() {
    return this.appState
  }
}

export default new AppStore()