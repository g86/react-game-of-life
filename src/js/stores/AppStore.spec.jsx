import React from 'react'
import Bibimbap from 'bibimbap'
import conf from '../constants/Constants'
import AppStore from '../stores/AppStore'

const initialCells = conf.CELLS

describe('Test AppStore', () => {

  describe('Store will use Bibimbap', () => {
    it('State will be an object and will contain initial cells', ()=> {
      expect(typeof AppStore.state).to.eql('object')
      expect(AppStore.state.cursor().get('cells')).to.eql(initialCells)
    })
    it('State will change on START event', ()=> {
      AppStore._registerToActions({actionType: 'START'})
      expect(AppStore.state.cursor().get('running')).to.eql(true)
    })
    it('Cells will mutate on MUTATE_GENERATION event', ()=> {
      AppStore._registerToActions({actionType: 'MUTATE_GENERATION'})
      expect(AppStore.state.cursor().get('cells')).not.to.eql(initialCells)
    })
    it('State will change on STOP event', ()=> {
      AppStore._registerToActions({actionType: 'STOP'})
      expect(AppStore.state.cursor().get('running')).to.eql(false)
    })
    it('State will reset on RESET event', ()=> {
      AppStore._registerToActions({actionType: 'RESET'})
      expect(AppStore.state.cursor().get('running')).to.eql(false)
      expect(AppStore.state.cursor().get('cells')).to.eql(initialCells)
    })
  })
})