import React from 'react'
import Bibimbap from 'bibimbap'
import conf from '../../constants/Constants'
import {GameOfLife} from './GameOfLife'
import {shallow} from 'enzyme'

const initialCells = conf.CELLS
let appStateMock = new Bibimbap({ cells: initialCells, running: false })

describe('Test "GameOfLife" component', () => {

  let component, instance //, theSpy

  beforeEach(()=> {
    // theSpy = sinon.spy()
    component = shallow(<GameOfLife appState={appStateMock}/>)
    instance = component.instance()
  })

  describe('Component will render', () => {
    it('it will contain two buttons', () => {
      expect(component.find("button").length).to.be.equal(2)
    })
    it('it will contain hr', () => {
      expect(component.find("hr").length).to.be.equal(1)
    })
    it('it will contain canvas', () => {
      expect(component.find("canvas").length).to.be.equal(1)
    })
  })

  describe('Component will receive cells via props', () => {

    it('initial cells will have 17 rows', () => {
      expect(instance.props.appState.cursor().get('cells').length).to.equal(17)
    })

    it('initial cells will have 17 columns', () => {
      expect(instance.props.appState.cursor().get('cells')[0].length).to.equal(17)
    })

    it('mounted component internal state as expected', () => {
      const columnsCount = instance.props.appState.cursor().get('cells')[0].length
      const rowsCount = instance.props.appState.cursor().get('cells').length
      const expectedState = {
        running: false,
        canvasWidth: columnsCount * conf.GRID_SIZE,
        canvasHeight: rowsCount * conf.GRID_SIZE
      }
      expect(instance.state).to.eql(expectedState)
    })

    it('initially app should not be running',()=>{
      expect(instance.props.appState.cursor().get('running')).to.eql(false)
      expect(instance.isRunning).to.eql(false)
    })
  })

  /*describe('Component will function', () => {
    it('Start/Stop should work', () => {
      instance.toggleRunning(theSpy)
      expect(instance.state.running).to.eql(true)
      instance.toggleRunning(theSpy)
      expect(instance.state.running).to.eql(false)
    })

    it('Reset should work', () => {
      instance.resetGame(theSpy)
      expect(instance.state.running).to.eql(false)
    })
  })*/

})