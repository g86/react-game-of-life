import React from 'react'
import nextGolGeneration from '../../helpers/GameOfLifeMutation'
import conf from '../../constants/Constants'

const cellSize = conf.CELL_SIZE
const gridSize = conf.GRID_SIZE
const lifeSpeed = conf.LIFE_SPEED
import AppDispatcher from '../../dispatchers/AppDispatcher'

export const getCanvasSize = (matrix) => {
  const height = conf.GRID_SIZE * (matrix.length)
  const width = conf.GRID_SIZE * (matrix[0].length)
  return {
    width,
    height
  }
}

export class GameOfLife extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      canvasWidth: 0,
      canvasHeight: 0
    }
    this.generationTimeout = null
  }

  get golCellsCursor() {
    return this.props.appState.cursor().get("cells")
  }

  get isRunning() {
    return this.props.appState.cursor().get("running")
  }

  drawToCanvas(data) {
    let canvas = this.refs.golCanvas
    let context = canvas.getContext('2d')
    let {width, height} = getCanvasSize(data)
    let blankImage = context.createImageData(width, height)
    context.putImageData(blankImage, 0, 0)

    if (data.length) {
      data.map((row, rowKey) => {
        row.map((cell, colKey) => {
          if (cell > 0) {
            context.fillStyle = "#f5f5f5"
            context.fillRect(colKey * gridSize, rowKey * gridSize, cellSize, cellSize)
          }
        })
      })
    }
  }

  componentWillMount() {
    const {width, height} = getCanvasSize(this.golCellsCursor)
    this.state = {
      canvasHeight: height,
      canvasWidth: width
    }
  }

  componentDidMount() {
    this.drawToCanvas(this.golCellsCursor)
  }

  askForNextGeneration() {
    AppDispatcher.dispatch({
      actionType: 'MUTATE_GENERATION'
    })
  }

  componentDidUpdate() {
    this.drawToCanvas(this.golCellsCursor)
    if (this.isRunning) {
      if (this.generationTimeout) clearTimeout(this.generationTimeout)
      this.generationTimeout = setTimeout(()=> {
        this.askForNextGeneration()
      }, lifeSpeed)
    } else {
      clearTimeout(this.generationTimeout)
    }
  }

  toggleRunning(event) {
    if (this.isRunning === true) {
      AppDispatcher.dispatch({
        actionType: 'STOP'
      })
    } else {
      AppDispatcher.dispatch({
        actionType: 'START'
      })
    }
  }

  resetGame(event) {
    clearTimeout(this.generationTimeout)
    AppDispatcher.dispatch({
      actionType: 'RESET'
    })
  }

  render() {
    let {canvasWidth, canvasHeight} = this.state

    const {width, height} = getCanvasSize(this.golCellsCursor)

    return (
      <div>
        <canvas ref="golCanvas"
                id="golArea"
                width={width}
                height={height}></canvas>
        <div>
          <button onClick={this.toggleRunning.bind(this)}>{this.isRunning ? 'Stop' : 'Start'}</button>
          <button onClick={this.resetGame.bind(this)}>Reset</button>
        </div>
      </div>
    )
  }
}

GameOfLife.propTypes = {
  appState: React.PropTypes.object
}

GameOfLife.defaultProps = {
  appState: []
}

export default GameOfLife