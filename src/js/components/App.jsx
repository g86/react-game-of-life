import React from 'react'
import GameOfLife from './GameOfLife/GameOfLife'
import AppStore from './../stores/AppStore'

export default class App extends React.Component {
  
  constructor() {
    super()
    this.state = {}
  }
  
  componentWillMount() {
    this._changeListener = this._onChange.bind(this)
    AppStore.addChangeListener(this._changeListener)
    this.state = AppStore.state
  }

  componentWillUnmount() {
    AppStore.removeChangeListener(this._changeListener)
  }
  
  _onChange() {
    let appState = AppStore.state
    this.setState(appState)
  }
  
  render() {
    const appState = AppStore.state // this.state
    return (
      <div id="golApp">
        <GameOfLife appState={appState} />
      </div>
    )
  }
}
