module.exports = (cells) => {

  let current_gen = cells,
    next_gen = [],
    length_y = cells.length,
    length_x,
    y, x
  // each row
  for (y = 0; y < length_y; y++) {
    length_x = current_gen[y].length
    next_gen[y] = []
    for (x = 0; x < length_x; x++) {
      let cell = current_gen[y][x]
      let row_above = (y - 1 >= 0) ? y - 1 : length_y - 1 // If current cell is on first row, cell "above" is the last row (stitched)
      let row_below = (y + 1 <= length_y - 1) ? y + 1 : 0 // If current cell is in last row, then cell "below" is the first row
      let column_left = (x - 1 >= 0) ? x - 1 : length_x - 1 // If current cell is on first row, then left cell is the last row
      let column_right = (x + 1 <= length_x - 1) ? x + 1 : 0 // If current cell is on last row, then right cell is in the first row

      let neighbours = {
        top_left: current_gen[row_above][column_left],
        top_center: current_gen[row_above][x],
        top_right: current_gen[row_above][column_right],
        left: current_gen[y][column_left],
        right: current_gen[y][column_right],
        bottom_left: current_gen[row_below][column_left],
        bottom_center: current_gen[row_below][x],
        bottom_right: current_gen[row_below][column_right]
      };

      let alive_count = 0
      let dead_count = 0
      for (let neighbour in neighbours) {
        if (neighbours[neighbour] == 0) {
          dead_count++
        } else {
          alive_count++
        }
      }
      // Set new state to current state, but it may change below
      let new_state = cell
      if (cell == 1) {
        if (alive_count < 2 || alive_count > 3) {
          // dead, overpopulation/underpopulation
          new_state = 0
        } else if (alive_count === 2 || alive_count === 3) {
          // lives on to next generation
          new_state = 1
        }
      } else {
        if (alive_count === 3) {
          // live, reproduction
          new_state = 1
        }
      }
      next_gen[y][x] = new_state
    }
  }
  return next_gen
}