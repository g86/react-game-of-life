import conf from '../constants/Constants'

module.exports = (mock) => {
  if (mock) return mock

  let maxWidth = window.innerWidth
  let maxHeight = window.innerHeight
  const margin = 15
  const navHeight = 50
  let colsCount = parseInt(( parseInt(maxWidth) - margin * 2 ) / conf.GRID_SIZE)
  let rowsCount = parseInt(( parseInt(maxHeight) - margin * 2 - navHeight ) / conf.GRID_SIZE)
  let matrix = []
  for (let y = 0; y < rowsCount; y++) {
    let row = []
    for (let x = 0; x < colsCount; x++) {
      // randomise a bit
      if ((x + y) * x / y % 3 == 0 || (((colsCount - x) * (rowsCount - y)) * 0.3 % 3 == 0 ) && x > 0.5 * colsCount && y > 0.5 * rowsCount) {
        row.push(1)
      } else {
        row.push(0)
      }
    }
    matrix.push(row)
  }
  return matrix
}