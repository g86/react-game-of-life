module.exports.getConfig = function(type) {

  	var isDev = type === 'development';

  	var config = {
		entry: [
			'./src/js/index.jsx'
		],
		module: {
			loaders: [
				{
					test: /\.jsx?$/,
					exclude: /node_modules/,
					loader: 'babel'
				}
			]
		},
		resolve: {
			extensions: ['','.js','.jsx']
		},
		output: {
			path: __dirname + '/src/js',
			publicPath: '/',
			filename: 'app.js'
		}
	};

	if(isDev){
    	config.devtool = 'eval';
  	}

  return config;
}