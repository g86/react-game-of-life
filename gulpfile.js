'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var mocha = require('gulp-mocha');
var open = require('gulp-open');
var os = require('os');
var rimraf = require('gulp-rimraf');
var path = require('path');
var $ = require('gulp-load-plugins')();
var notify = require('gulp-notify');
var babel = require('babel-core/register');
var htmlreplace = require('gulp-html-replace');

// set variable via $ gulp --type production
var environment = $.util.env.type || 'development';
var isProduction = environment === 'production';
var compressScripts = environment != 'development';
var webpackConfig = require('./webpack.config.js').getConfig(environment);

var port = $.util.env.port || 3003;
var app = 'src/';
var dist = 'dist/';

var autoprefixerBrowsers = [
  'ie >= 9',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 6',
  'opera >= 23',
  'ios >= 6',
  'android >= 4.4',
  'bb >= 10'
];

// tasks definition

gulp.task('clean', function () {
  return gulp.src([
    __dirname + '/dist/*'
  ]).pipe(rimraf());
});

gulp.task('test', function() {
  return gulp.src(['test-helpers.js', 'src/js/**/*.spec.js', 'src/js/**/*.spec.jsx'], { read: false })
    .pipe(mocha({
      reporter: 'nyan',
      compilers: {
        js: babel
      },
      require: [
        'mock-local-storage'
      ]
    }))
    .on('error', gutil.log);
});

gulp.task('styles', function () {
  return gulp.src(__dirname + '/src/assets/scss/app.scss')
             .pipe($.plumber())
             .pipe($.sass({errLogToConsole: true, compress: compressScripts, 'include css': true}))
             .pipe($.autoprefixer(autoprefixerBrowsers))
             .pipe($.flatten())
             .pipe($.csso())
             .pipe(gulp.dest(__dirname + '/dist/css'))
             .pipe($.size({title: 'css'}))
             .pipe($.connect.reload())
             .pipe($.notify({
               message: 'SCSS compilation complete.',
               notifier: function () {
                 return false;
               }
             }));
});

gulp.task('images', function () {
  return gulp.src([
               __dirname + '/src/assets/images/**/*.{png,jpg,jpeg,gif,svg}',
               __dirname + '/src/assets/images/*.{png,jpg,jpeg,gif,svg}'
             ])
             .pipe($.plumber())
             .pipe($.rename(function (path) {
               var temp = path.dirname.split("/");
               temp.splice(0, 4);
               path.dirname = temp.toString().replace(/,/g, "/");
             }))
             .pipe(gulp.dest(__dirname + '/dist/images'))
             .pipe($.size());
});

gulp.task('scripts', function () {
  return gulp.src(webpackConfig.entry)
             .pipe($.webpack(webpackConfig))
             .pipe(compressScripts ? $.uglify() : $.util.noop())
             .pipe(gulp.dest(dist + 'js/'))
             .pipe($.size({title: 'js'}))
             .pipe($.connect.reload());

});

// copy html from app to dist
gulp.task('html', function () {
  var date = new Date();
  var version = date.getTime();
  console.log("Application build timestamp: " + version);
  return gulp.src(app + 'index.html')
             .pipe(htmlreplace({
               'css': {
                 src: null,
                 tpl: '<link media="all" rel="stylesheet" href="/css/app.css?v=' + version + '" />',
               },
               'js': {
                 src: null,
                 tpl: '<script src="/js/app.js?v=' + version + '" type="text/javascript"></script>'
               }
             }))
             .pipe(gulp.dest(dist))
             .pipe($.size({title: 'html'}))
             .pipe($.connect.reload());
});

gulp.task('openapp', function () {

  // var browser = 'google chrome'; // mac
  // var browser = 'chrome'; // win

  var browser = os.platform() === 'linux' ? 'google-chrome' : (
    os.platform() === 'darwin' ? 'google chrome' : (
      os.platform() === 'win32' ? 'chrome' : 'firefox'));

  // only for openning the app
  var options = {
    // uri: 'http://coding-test.scandit.com:' + port,
    uri: 'http://localhost:' + port,
    app: browser
  };

  return gulp.src(__filename)
             .pipe(open(options));
});

// add livereload on the given port
gulp.task('serve', function () {
  $.connect.server({
    root: dist,
    port: port,
    livereload: {
      port: 35729
    }
  });
  // gulp.start(['openapp']);
});

// watch styl, html and js file changes
gulp.task('watch', function () {
  gulp.watch(app + 'assets/scss/*.scss', ['styles']);
  gulp.watch(app + 'assets/scss/**/*.scss', ['styles']);
  gulp.watch(app + 'index.html', ['html']);
  gulp.watch(app + 'js/*.js', ['scripts']);
  gulp.watch(app + 'js/*.jsx', ['scripts']);
  gulp.watch(app + 'js/**/*.js', ['scripts']);
  gulp.watch(app + 'js/**/*.jsx', ['scripts']);
});

// waits until clean is finished then builds the project
gulp.task('build', ['clean'], function () {
  return gulp.start(['images', 'html', 'scripts', 'styles', 'images']);
});

gulp.task('default', ['build'], function () {
  return gulp.start(['serve', 'watch', 'openapp']);
  return true;
});